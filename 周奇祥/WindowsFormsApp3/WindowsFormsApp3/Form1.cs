﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            var abc = "select * from Students";
            var dt = DbHelper.GetDataTable(abc);
            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }
        //查找
        private void button1_Click_1(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where StudentName like '%{0}%'", name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;
        }

        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            EditFrom form = new EditFrom();
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var abc = "select * from Students";

                var dt = DbHelper.GetDataTable(abc);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        //更新
        private void button3_Click_1(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var grade = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
            EditFrom form = new EditFrom(id, name, age, grade);
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from Students";
                var dt = DbHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("更新错误");
            }
        }
        //删除
       
        private void button4_Click_1(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var sql = string.Format("delete from Students where Id={0}", id);
                var res = DbHelper.AddOrUpadteOrDelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功");
                    var abc = "select * from Students";
                    var dt = DbHelper.GetDataTable(abc);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("操作错误");
                }
            }
        }
    }
}
