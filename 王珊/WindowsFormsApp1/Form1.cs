﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql = "select * from Students";
            var dt = DBHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;
           
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dataGridView1.ReadOnly = true;

            dataGridView1.AllowUserToAddRows = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Students where stuName like '%{0}%'", name);
            var dt = DBHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from Students";
                var dt = DBHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }

        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var stuId = (int)dataGridView1.SelectedRows[0].Cells["stuId"].Value;
            var stuName = (string)dataGridView1.SelectedRows[0].Cells["stuName"].Value;
            var stuAge = (int)dataGridView1.SelectedRows[0].Cells["stuAge"].Value;
            var stuScore = (int)dataGridView1.SelectedRows[0].Cells["stuScore"].Value;
            Form2 form2 = new Form2(stuId, stuName, stuAge, stuScore);
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from Students";
                var dt = DBHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var stuId = (int)dataGridView1.SelectedRows[0].Cells["stuId"].Value;
                var sql = string.Format("delete from Students where stuId={0}", stuId);
                var res = DBHelper.AddOrUpdateOrDelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    var stusql = "select * from Students";
                    var dt = DBHelper.GetDataTable(stusql);
                    dataGridView1.DataSource = dt;

                }
                else
                {
                    MessageBox.Show("删除失败", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
                }
            }

            else
            {
                MessageBox.Show("当前未选择要删除的数据", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
            }

        }
    }
    }

