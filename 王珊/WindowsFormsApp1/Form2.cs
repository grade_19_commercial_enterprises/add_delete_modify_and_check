﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private int stuId;
        public Form2(int stuId, string stuName, int stuAge, int stuScore)
        {
            InitializeComponent();
            this.stuId = stuId;
            textBox1.Text = stuName;
            textBox2.Text = stuAge.ToString();
            textBox3.Text = stuScore.ToString();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var stuName = textBox1.Text;
            var stuAge = textBox2.Text;
            var stuScore = textBox3.Text;
            //更新
            if (this.stuId > 0)
            {
                var sql = string.Format("update Students set stuName='{0}',stuAge={1},stuScore={2} where stuId={3}", stuName, stuAge, stuScore, this.stuId);
                DBHelper.AddOrUpdateOrDelete(sql);
                MessageBox.Show("更新成功", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            }
            //添加
            else
            {
                var sql = string.Format("insert into Students (stuName,stuAge,StuScore) values ('{0}',{1},{2})", stuName, stuAge, stuScore);
                DBHelper.AddOrUpdateOrDelete(sql);
                MessageBox.Show("添加成功", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            }
            this.DialogResult = DialogResult.Yes;
            this.Hide();
        }
    }
    }
