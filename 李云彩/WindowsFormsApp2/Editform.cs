﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Editform : Form
    {
        private static int Id;
        public Editform()
        {
            InitializeComponent();

        }
        public Editform(int id, string name, double credit, string remark)
        {
            InitializeComponent();
            Id = id;
            textBox1.Text = name;
            textBox2.Text = credit.ToString();
            textBox3.Text = remark;
        }


        //确认
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var credit = textBox2.Text;
            var remark = textBox3.Text;
            //修改
            if (Id > 0)
            {
                if (!string.IsNullOrEmpty(name))
                {
                    var sql = string.Format("update course set name='{0}',credit='{1}',remark='{2}' where id = {3}", name, credit, remark, Id);
                    var res = DbHelper.AddOrEditSave(sql);
                    if (res == 1)
                    {
                        MessageBox.Show("修改成功");
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("修改失败");
                        this.Close();
                    }

                }
                //添加
                else
                {
                    this.DialogResult = DialogResult.No;
                    this.Close();
                }
            }
            else
            {

                if (!string.IsNullOrEmpty(name))
                {
                    var sql = string.Format("insert into course (name, credit, remark) values ('{0}',{1},'{2}');", name, credit, remark);
                    var res = DbHelper.AddOrEditSave(sql);
                    if (res == 1)
                    {
                        MessageBox.Show("添加成功");
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("添加失败");
                        this.Close();
                    }

                }
                else
                {
                    MessageBox.Show("你未填写任何数据", "提示");
                }


            }
        }
        //取消
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
