﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //查询
        private void button1_Click(object sender, EventArgs e)
        {
            var query = textBox1.Text;
            var sqlquery = string.Format("select * from course where name like'%{0}%'", query);
            var sdf = DbHelper.GetdataTable(sqlquery);
            dataGridView1.DataSource = sdf;
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Editform editform = new Editform();
            var res = editform.ShowDialog();

            if (res == DialogResult.Yes)
            {
                string codString = "select * from course";
                var dataTable = DbHelper.GetdataTable(codString);
                dataGridView1.DataSource = dataTable;
            }
            else
            {

            }
        }
        //修改
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
                var name = (string)dataGridView1.SelectedRows[0].Cells["name"].Value;
                var credit = (double)dataGridView1.SelectedRows[0].Cells["credit"].Value;
                var remark = (string)dataGridView1.SelectedRows[0].Cells["remark"].Value;
                Editform editform = new Editform(id, name, credit, remark);
                var res = editform.ShowDialog();
                if (res == DialogResult.Yes)
                {
                    string codString = "select * from course";
                    var dataTable = DbHelper.GetdataTable(codString);
                    dataGridView1.DataSource = dataTable;

                }
                else
                {

                }

            }
            else
            {
                MessageBox.Show("请选择需要更改的行", "提示");
            }
        }
         //删除
        private void button4_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                var row = dataGridView1.SelectedRows[0];
                var id = (int)row.Cells["id"].Value;
                var res = MessageBox.Show("确定删除吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);


                if (res.Equals(DialogResult.Yes))
                {

                    var str = string.Format("delete from course where id = {0}", id);
                    var count = DbHelper.AddOrEditSave(str);


                    if (count > 0)
                    {
                        MessageBox.Show("删除成功", "提示");
                        string codString = "select * from course";
                        var dataTable = DbHelper.GetdataTable(codString);
                        dataGridView1.DataSource = dataTable;
                    }
                }
            }
            else
            {
                MessageBox.Show("你没有选中任何数据", "提示");
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            string codString = "select * from course";
            var dataTable = DbHelper.GetdataTable(codString);
            dataGridView1.DataSource = dataTable;
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.Columns[0].HeaderText = "编号";
            dataGridView1.Columns[1].HeaderText = "课程名称";
            dataGridView1.Columns[2].HeaderText = "学分";
            dataGridView1.Columns[3].HeaderText = "备注";
            dataGridView1.BackgroundColor = Color.White;
        }
    }
}
