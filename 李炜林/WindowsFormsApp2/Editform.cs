﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Editform : Form
    {
        public Editform()
        {
            InitializeComponent();
        }


        private int id;
        //private string stuName;
        //private int age;
        //private int score;
        public Editform(int id, string stuName, int age, int score)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = stuName;
            textBox2.Text = age.ToString();
            textBox3.Text=score.ToString();

            
        }


        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //更新
        private void button1_Click(object sender, EventArgs e)
        { 
                var name = textBox1.Text;
                var age = textBox2.Text;
                var score = textBox3.Text;

            if (this.id > 0)
            {
                var sql = string.Format("update student set stuName='{0}',age={1},score={2} where Id={3}", name, age, score, this.id);

                Dbhelper.AddorUpdate(sql);

                MessageBox.Show("更新成功！", "提示");
            }

            //添加
            else 
            {
                var sql = string.Format("insert into student (stuName,age,score) values ('{0}',{1},{2})", name, age, score);
                Dbhelper.AddorUpdate(sql);

                MessageBox.Show("添加成功！", "提示");
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
