﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“dbxDataSet.student”中。您可以根据需要移动或删除它。
            //this.studentTableAdapter.Fill(this.dbxDataSet.student);

            var abc = "select * from student";

            var stuSql = "select * from class";

            var dt = Dbhelper.GetDataTable(abc);

            dataGridView1.DataSource = dt;

            //限制dataGridView1的某些行为

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;//整行单元格

            dataGridView1.ReadOnly = true;//禁止编辑

            dataGridView1.AllowUserToAddRows = false;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void 查找_Click(object sender, EventArgs e)
        {

            var name = textBox1.Text;

            var sql = string.Format("select * from student where stuName like '%{0}%'",name);

            var dt = Dbhelper.GetDataTable(sql);

            dataGridView1.DataSource=dt;



        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Editform form = new Editform();
           var res= form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from class";

                var dt = Dbhelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;


            }
            else 
            {
                MessageBox.Show("No");

            }
        }
        /// <summary>
        ///更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {

            var id =(int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var stuName = (string)dataGridView1.SelectedRows[0].Cells["stuName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["score"].Value;

            Editform form = new Editform(id,stuName,age,score);
             var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {

                var stuSql = "select * from class";

                var dt = Dbhelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;


            }
            else 
            {
                MessageBox.Show("No");



            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            // var row = dataGridView1.SelectedRows[0];

            //  var cell = row.Cells[0];

            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

            var sql = string.Format("delete from student where id={0}", id);

            this.DialogResult = DialogResult.Yes;

            if (MessageBox.Show("是否删除？", "提示", MessageBoxButtons.YesNo) == this.DialogResult)
            {
                var res = Dbhelper.AddorUpdate(sql);
                if (res > 0)
                {
                    MessageBox.Show("删除成功！", "提示");

                    var fff = "select * from student";

                    var dt = Dbhelper.GetDataTable(fff);

                    dataGridView1.DataSource = dt;
                }

            }
            else 
            {
                MessageBox.Show("删除失败！", "提示");

                var fff = "select * from student";

                var dt = Dbhelper.GetDataTable(fff);

                dataGridView1.DataSource = dt;

            }

         

           

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}