﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“masterDataSet.Students”中。您可以根据需要移动或删除它。

            var abc = "select * from Students";
            var teaSql = "select * from Teachers";
            var dt = DbHelper.GetDataTable(abc);
            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var name = label1.Text;
            var sql = string.Format("select *from Students  where StudentName like '%_桑'", name);
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //添加
            EditForm form = new EditForm();
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from students";
                var dt = DbHelper.GetDataTable(stuSql);
                dataGridView1.DataSource = dt;

            }
            else
            {

                MessageBox.Show("No");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //更新
            var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells[3].Value;

            EditForm form = new EditForm(id, name, age, score);
            var res = form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from students";
                var dt = DbHelper.GetDataTable(stuSql);
                dataGridView1.DataSource = dt;

            }
            else
            {
                MessageBox.Show("No");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //删除


            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var sql = string.Format("delete from Studentinfo where Id={0}", id);
                var res = DbHelper.AddOrUpadteOrDelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功");
                    var abc = "select * from Studentinfo";
                    var dt = DbHelper.GetDataTable(abc);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("操作错误");


                }
            }
        }
    }
}
