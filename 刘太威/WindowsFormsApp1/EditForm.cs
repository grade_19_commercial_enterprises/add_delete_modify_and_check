﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }
        private int id;
        
      

        public EditForm(int id,string name,int age,int score)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString ();

          
        }
        

       

        private void button1_Click(object sender, EventArgs e)
        {//确认保存
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;
            if(this.id>0)
            {
                var sql = string.Format("update Students set StudentsName='{1}',Age{2},Score{3} where Id={0}", name ,age ,score,this.id);
                DbHelper.AddorUpdate(sql);
                MessageBox.Show("更新成功");

            }
            else
            {
                var sql = string.Format("insert into Students(StudentName,Age,Score) values('{1}',{2},{3})", name, age, score);
                DbHelper.AddorUpdate(sql);
                MessageBox.Show("添加成功");

            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
            
          
            
        }
        private void button2_Click(object sender, EventArgs e)
        {//取消
            this.DialogResult = DialogResult.No;
            this.Close();
        }

    }
}
