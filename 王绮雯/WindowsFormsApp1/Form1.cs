﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var abc = "select * from specialty";

            var stuSql = "select * from students";

            var dt = DbHelper.GetDataTable(stuSql);

            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;


        }
        /// <summary>
        /// 查找
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            var sql = string.Format("select * from Students where StudentName like '%{0}%'",name);



            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource=dt;

            
        }


        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res=form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var abc = "select * from Students";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;

            EditForm form = new EditForm(id,name,age,score);


            var res=form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var abc = "select * from Students";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {

            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

                var sql = string.Format("delete from Students where Id={0}", id);

                this.DialogResult = DialogResult.Yes;
                if (MessageBox.Show("是否删除", "提示", MessageBoxButtons.YesNo) == this.DialogResult)
                {
                    var res = DbHelper.AddOrUpdateOrDelete(sql);
                    if (res > 0)
                    {
                        MessageBox.Show("删除成功", "提示");

                        var abc = "select * from Students";
                        var dt = DbHelper.GetDataTable(abc);

                        dataGridView1.DataSource = dt;
                    }

                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                    var abc = "select * from Students";
                    var dt = DbHelper.GetDataTable(abc);

                    dataGridView1.DataSource = dt;
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据","提示");
            }

            
            

           
        }
    }
}
