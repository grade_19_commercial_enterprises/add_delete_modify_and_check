﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“sterDataSet.Stsdents”中。您可以根据需要移动或删除它。
            //this.stsdentsTableAdapter.Fill(this.sterDataSet.Stsdents);
            var sql = "select * from Stsdents ";

           var dt= DbHelper.FangPrint(sql);




            dataGridView1.DataSource = dt;
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;

            
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count>0)
            {
                var row = dataGridView1.SelectedRows[0];


                var id = (int)row.Cells[0].Value;

                var res = MessageBox.Show("确定删除吗？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                if (res.Equals(DialogResult.Yes))
                {
                    var sql = string.Format("delete from Stsdents where id={0}", id);
                    var rowCount = DbHelper.AddOrEdiSave(sql);
                    if (rowCount > 0)
                    {
                        var sqlStu = "select * from Stsdents";
                        var dt = DbHelper.FangPrint(sqlStu);

                        dataGridView1.DataSource = dt;

                        MessageBox.Show("删除成功","提示");
                    }
                }

            }
            else
            {
                MessageBox.Show("没有选择任何的行", "提示");

            }

        }


        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button1_Click(object sender, EventArgs e)
       {

            var Name = textBox1.Text;
            var sql = string.Format("select * from Stsdents where Name like '%{0}%'", Name);
            var dt = DbHelper.FangPrint(sql);

            dataGridView1.DataSource = dt;
        }
        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button2_Click(object sender, EventArgs e)
        {   

            Form4 form4 = new Form4();
            form4.Show();




        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public int id;
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                var Name = (string)dataGridView1.SelectedRows[0].Cells[1].Value;
                var age = (int)dataGridView1.SelectedRows[0].Cells[2].Value;
                Form3 form = new Form3(id,Name,age);
                form.ShowDialog();

            }
            else
            {
                MessageBox.Show("您未选择任何的数据行","提示");
            }

          
           

            




        }
    }
}
