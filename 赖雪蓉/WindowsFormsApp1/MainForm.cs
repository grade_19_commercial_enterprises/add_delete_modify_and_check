﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Student where StudentName like '%{0}%'", name);

            var db = DbHelp.GetDataTable(sql);

            dataGridView1.DataSource = db;
        }

        /// <summary>
        /// 增加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from student";

                var dt = DbHelp.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
             if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

                var sql = string.Format("delete from Students where Id={0}", id);

                var res = DbHelp.AddOrUpdateOrDelete(sql);

                if (res == 1)
                {
                    MessageBox.Show("删除成功！", "提示");

                    var stuSql = "select * from students";

                    var dt = DbHelp.GetDataTable(stuSql);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败！", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据","提示");
            }

        }


        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
            EditForm form = new EditForm(id, name, age, score);
            var res = form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from students";

                var dt = DbHelp.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            var abc = "select * from user";

            var stuSql = "select * from student";

            var dt = DbHelp.GetDataTable(stuSql);

            dataGridView1.DataSource = dt;

            
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;


          
        }
    }
}
