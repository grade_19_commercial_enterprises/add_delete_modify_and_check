﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class EditForm : Form
    {
        private int id;
        private int age;
        private int score;

        public EditForm()
        {
            InitializeComponent();
        }

        public EditForm(int id, string name, int age, int score)
        {
            this.id = id;
            Name = name;
            this.age = age;
            this.score = score;
        }
    }
}
