create database Student

use Student
go
create table Studentinfo
(
	Id int primary key identity,
	StudentName nchar(20) not null,
	Score int not null,
	Age int not null
)
go

select * from Studentinfo

insert into Studentinfo values('张三','1','40')
insert into Studentinfo values('李四','2','30')
insert into Studentinfo values('王五','3','20')
insert into Studentinfo values('刘六','4','10')

