﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string sql;
        private int resCount;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // TODO: 这行代码将数据加载到表“studentDataSet1.Studentinfo”中。您可以根据需要移动或删除它。

            var stuSql = "select * from Studentinfo";

            var dt = DbHelper.GetDataTable(stuSql);

            dataGridView1.DataSource = dt;

            //限制DataGridView的某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from Studentinfo";

                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var name = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
                var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
                var grade = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
                EditForm form = new EditForm(id, name, age, grade);
                var res = form.ShowDialog();
                if (res == DialogResult.Yes)
                {
                    var stuSql = "select * from Studentinfo";

                    var dt = DbHelper.GetDataTable(stuSql);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("No");
                }
            }
            else
            {
                MessageBox.Show("你没有选择任何数据", "提示");
            }

        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows;

            if (row.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

                var sql = string.Format("delete from Studentinfo where Id={0}", id);

                var res = DbHelper.AddOrUpadteOrDelete(sql);

                if (resCount>0)
                {
                    MessageBox.Show("删除成功！", "提示");

                    var stuSql = "select * from Studentinfo";

                    var dt = DbHelper.GetDataTable(stuSql);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败！", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据", "提示");
            }

          
        }
        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Studentinfo where StudentName like '%{0}%'", name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;

        }
    }
}
