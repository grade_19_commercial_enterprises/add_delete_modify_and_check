﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }

        private int Id;
       

        public EditForm(int id, string name, int age, int score)
        {
            InitializeComponent();


            this.Id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();
         }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;

            //更新
            if (this.Id > 0)
            {
                var sql = string.Format("update Studentinfo set StudentName='{0}',Age={1},Score={2} where Id={3}",name,age,score,this.Id);

                DbHelper.AddOrUpadteOrDelete(sql);

                MessageBox.Show("更新成功！", "提示");
            }
            //添加
            else
            {
                var sql = string.Format("insert into Studentinfo(StudentName,Age,Score)values('{0}',{1},{2})", name, age, score);
                DbHelper.AddOrUpadteOrDelete(sql);

                MessageBox.Show("添加成功！", "提示");

            }

            this.DialogResult = DialogResult.Yes;
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
                                                                       
        }
       
    }
}
