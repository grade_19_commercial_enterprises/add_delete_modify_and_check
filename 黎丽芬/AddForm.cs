﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADUQ
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
        }
        private int id;

        public AddForm (int id,string name,int price,long BC)
        {
            InitializeComponent();

            this.id = id;
            textBox1.Text = name;
            textBox2.Text = price.ToString();
            textBox3.Text = BC.ToString();
            
        }
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var price = textBox2.Text;
            var BC = textBox3.Text;

            //更新
           if(this.id > 0)
            {
                var sql = string.Format("update Commodity set CommodityName='{0}',price={1},BarCode={2} where id={3}",name,price,BC,this.id);

                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("更新成功", "提示");
            }
           //添加
           else
            {
                var sql = string.Format("insert into Commodity(CommodityName,price,BarCode)values ('{0}',{1},{2})", name, price, BC);
                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("添加成功", "提示");
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
