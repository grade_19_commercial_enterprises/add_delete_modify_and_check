﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ADUQ
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“merchandiseDataSet.Commodity”中。您可以根据需要移动或删除它。
            var inquire = "select * from Commodity";

            var dt = DbHelper.GetDataTable(inquire);

            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            //查询
            var name = textBox1.Text;

            var sql = string.Format("select * from Commodity where CommodityName like '%{0}%'",name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //添加
            AddForm form = new AddForm();
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var inquire = "select * from Commodity";

                var dt = DbHelper.GetDataTable(inquire);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("请确认是否要取消添加","提示");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //更新
            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["CommodityName"].Value;
            var price = (int)dataGridView1.SelectedRows[0].Cells["price"].Value;
            var BC = (long)dataGridView1.SelectedRows[0].Cells["BarCode"].Value;

            AddForm form = new AddForm(id,name,price ,BC);

            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var inquire = "select * from Commodity";

                var dt = DbHelper.GetDataTable(inquire);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("请确认是否要取消更新","提示");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // 删除
            var row = dataGridView1.SelectedRows[0];

            var cell = row.Cells[0];

            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;

            var sql = string.Format("delete from Commodity where id={0}",id);

            
        }
    }
}
