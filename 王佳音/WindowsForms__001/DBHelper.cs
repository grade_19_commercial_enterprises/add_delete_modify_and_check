﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms__001
{
   public  class DBHelper
    {
        private static readonly string connString = "server=.;database=test9;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(connString);
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            connection.Open();
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }
        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            return command.ExecuteNonQuery();

        }
    }
}
