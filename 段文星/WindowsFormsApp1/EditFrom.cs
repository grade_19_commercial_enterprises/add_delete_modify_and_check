﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class EditFrom : Form
    {
        public EditFrom()
        {
            InitializeComponent();
        }
        private int id;

        public EditFrom(int id, string name, int age, int score)
        {
            InitializeComponent();
            this.id = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();
            //InitializeComponent();
        }  

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;
            if (this.id > 0)
            {
                var sql = string.Format("update Students set  StudentName='{0}',Age={1},Score={2} where Id={3}", name, age, score, this.id);
                DbHelper.AddOrUpdateOrDelect(sql);
                MessageBox.Show("更新成功", "提升");
            }
            else
            {
                var sql = string.Format("insert into students (StudentName,Age,Score) values ('{0}','{1}','{2}')", name, age, score);
                DbHelper.AddOrUpdateOrDelect(sql);
                MessageBox.Show("添加成功", "提示");
 
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
