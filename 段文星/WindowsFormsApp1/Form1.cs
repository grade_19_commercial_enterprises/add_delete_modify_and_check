﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“duanDataSet.Students”中。您可以根据需要移动或删除它。
            var abc = "select*from Students";

            var dt = DbHelper.GetDataTable(abc);
            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox2.Text;
            var sql = string.Format("select*from Students where StudentName like '%{0}%'",name);
            var da = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = da;
        }
        //添加
        private void button3_Click(object sender, EventArgs e)
        {
            EditFrom form = new EditFrom();
            var res = form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var abc = "select*from Students";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("添加失败！", "提升");
            }
        }
       
        //删除
        private void button7_Click(object sender, EventArgs e)
        {
            if(dataGridView1.SelectedRows.Count>0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var sql = string.Format("delete from Students where Id={0}", id);
                var res = DbHelper.AddOrUpdateOrDelect(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功");
                    var a = "select * from Students ";
                    var dt = DbHelper.GetDataTable(a);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("操作错误");


                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据", "提示");
            }
           
        }
        //更新
        private void button4_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;

            EditFrom from = new EditFrom(id, name, age, score);
            var res = from.ShowDialog();
            if(res==DialogResult.Yes)
            {
                var abc = "select * from students";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }
        }
    }
}
    


