﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        private int id;

        public Form2()
        {
            InitializeComponent();
        }

       
       


        public Form2(int id, string name, int age, string sex,double yj)
        {
            InitializeComponent();

            this.id = id;

            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox4.Text = sex;
           textBox3.Text = yj.ToString();
        }



        private void Form2_Load(object sender, EventArgs e)
        {

        }


       


        /// <summary>
        /// 取消
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }



        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var sex = textBox4.Text;
            var yj = textBox3.Text;


            //修改
            if (this.id > 0)
            {
                var aaa = string.Format("update staff set name = '{0}', age = '{1}', sex = '{2}', yj = '{3}' where id = '{4}'",name,age,sex,yj,this.id ) ;
                Class1.Addupdate(aaa);

                MessageBox.Show("修改成功", "提示");
            }



            //添加
            else
            {
                var aaa = string.Format("insert into staff values('{0}', '{1}', '{2}', '{3}')", name, age, sex, yj);
                Class1.Addupdate(aaa);

                MessageBox.Show("添加成功","恭喜");
            }


            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
