﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
             var sql = "select*from staff";

            var dt = Class1.GetDataTable(sql);

            dataGridView1.DataSource = dt;

            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //var name = textBox1.Text;
            //var cha = string.Format("select*from staff where name like '%{0}%'", name);

            //var dt = Class1.GetDataTable(cha);

            //dataGridView1.DataSource = dt;


            if (string.IsNullOrEmpty(textBox1.Text))
            {
                string aaa = "select *from staff";
                var dt = Class1.GetDataTable(aaa);
                dataGridView1.DataSource = dt;
                MessageBox.Show("查找内容不能为空", "提示");
                textBox1.Focus();
            }
            else
            {
                var name = textBox1.Text;
                var sql = string.Format("select*from staff where name like '%{0}%'",name);
                var dt = Class1.GetDataTable(sql);
                dataGridView1.DataSource = dt;
                textBox1.Focus();
                textBox1.SelectAll();
            }

            
        }


        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
           

            var res = form2.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var sql = "select*from staff";

                var dt = Class1.GetDataTable(sql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");

            }
        }




        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
           

            var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["name"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["age"].Value;
            var sex = (string)dataGridView1.SelectedRows[0].Cells["sex"].Value;
            var yj = (double)dataGridView1.SelectedRows[0].Cells["yj"].Value;

            Form2 form2 = new Form2(id,name,age,sex,yj);


            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select*from staff";

                var dt = Class1.GetDataTable(sql);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("您未修改");
            }

        }



        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            

                var rows = dataGridView1.SelectedRows;

                if (rows.Count > 0)
                {
                    var id = (int)rows[0].Cells["id"].Value;

                    var aaa = string.Format("delete from staff where id={0}",id);

                var resCount = Class1.Addupdate(aaa);

                if (resCount > 0)
                {
                    MessageBox.Show("删除成功","提示");
                    
                }
                else
                {
                    MessageBox.Show("不好意思，删除失败", "提示");
                }
                }
                else
                {
                    MessageBox.Show("请选择要删除的一行数据","提示");
                }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
