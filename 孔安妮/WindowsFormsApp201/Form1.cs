﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp201
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select *from Students where studentName like '%{0}%'", name);
            var dt = Dbhelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select *from Students";
                var dt = Dbhelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var Id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var studentName = (string)dataGridView1.SelectedRows[0].Cells["studentName"].Value;
            var Age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var Score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
           
            Form2 form2 = new Form2(Id, studentName, Age, Score);

            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select *from Students";
                var dt = Dbhelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }

        }
        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var Id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var sql = string.Format("delete from Students where Id={0}", Id);
                var res = Dbhelper.AddOrUpdateOrDelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    var stusql = "select *from Students";
                    var dt = Dbhelper.GetDataTable(stusql);
                    dataGridView1.DataSource = dt;

                }
                else
                {
                    MessageBox.Show("删除失败", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
                }
            }

            else
            {
                MessageBox.Show("当前未选择要删除的数据", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql = "select *from Students";
            var dt = Dbhelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }
    }
    
}
