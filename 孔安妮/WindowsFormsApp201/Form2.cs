﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp201
{
    public partial class Form2 : Form

    {
        private int Id;
        private string studentName;
        private int Age;
        private int Score;

        public Form2()
        { 
            InitializeComponent();
        }
        public Form2(int Id, string studentName, int Age, int Score)
        {
            InitializeComponent();
            this.Id = Id;
            this.studentName = studentName;
            this.Age = Age;
            this.Score = Score;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            textBox1.Text = this.studentName;
            textBox3.Text = this.Age.ToString();
            textBox2.Text = this.Score.ToString();





        }

        private void button1_Click(object sender, EventArgs e)
        {
            var studentName = textBox1.Text;
            var Age = textBox3.Text;
            var Score = textBox2.Text;
            //更新
            if (this.Id > 0)
            {
                var sql = string.Format("update Students set studentName='{0}',Age={1},Score={2} where Id={3}", studentName, Age, Score, this.Id);
                Dbhelper.AddOrUpdateOrDelete(sql);
                MessageBox.Show("更新成功", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            }
            //添加
            else
            {
                var sql = string.Format("insert into Students (studentName,Age,Score) values ('{0}',{1},{2})", studentName, Age, Score);
                Dbhelper.AddOrUpdateOrDelete(sql);
                MessageBox.Show("添加成功", "标题", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);
            }
            this.DialogResult = DialogResult.Yes;
            this.Hide();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}
