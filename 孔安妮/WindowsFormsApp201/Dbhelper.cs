﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp201
{
   public class Dbhelper
    {

        private static readonly string connString = "server=.;database=Admin;uid=sa;pwd=123456789;";

        public static DataTable GetDataTable(string sql)
        {
            System.Data.SqlClient.SqlConnection connection = new SqlConnection(connString);
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            connection.Open();
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }
        public static int AddOrUpdateOrDelete(string sql)
        {
            SqlConnection connection = new SqlConnection(connString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            return command.ExecuteNonQuery();

        }
    }
}

 