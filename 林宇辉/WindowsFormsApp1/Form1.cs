﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       
       

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var abc = "select * from Study";
            var stuSql = "select * from Students";
            var dt = DbHelper.GetDataTable(stuSql);
            
            dataGridView1.DataSource = dt;
            //限制DataGridVire的某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select* from Students where StudentName like'%{0}%'",name);
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
            EditForm form = new EditForm();
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from Students";
                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
    
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
            EditForm form = new EditForm(id, name, age, score);
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var stuSql = "select * from Students";
                var dt = DbHelper.GetDataTable(stuSql);

                dataGridView1.DataSource = dt;
            }
  
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var row = dataGridView1.SelectedRows[0];
                var cell = row.Cells[0];
                var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var sql = string.Format("delete from Students where Id={0}", id);
                var res = DbHelper.AddOrUpdateOrDelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");
                    var stuSql = "select * from Students";
                    var dt = DbHelper.GetDataTable(stuSql);

                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
            }
            else {
                MessageBox.Show("请选择需要删除的数据");
            }
           
        }//删除
    }
} 
