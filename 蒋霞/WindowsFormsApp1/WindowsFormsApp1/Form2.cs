﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        int UserId1;
        public Form2()
        {
            InitializeComponent();
            button1.Text = "添加";
        }
        public Form2(int UserId, string UserName, string UserAge)
        {
            InitializeComponent();
            button1.Text = "修改";
            textBox1.Text = UserName;
            textBox2.Text = UserAge;
            UserId1 = UserId;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text.Equals("修改"))
            {
                var sql = string.Format("update Users set UserName='{0}',UserAge={1} where UserId={2}", textBox1.Text, textBox2.Text, UserId1);
                Dbsql.aud(sql);
                MessageBox.Show("修改成功", "提示");
            }
            else  if (button1.Text.Equals("添加"))
            {
                var sql = string.Format("insert into Users (UserName,UserAge) values ('{0}',{1})", textBox1.Text, textBox2.Text);
                Dbsql.aud(sql);
                MessageBox.Show("添加成功", "提示");
            }
            this.DialogResult = DialogResult.Yes;
            this.Hide();
        }
    }
}
