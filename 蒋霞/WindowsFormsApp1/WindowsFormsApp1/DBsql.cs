﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Dbsql
    {
        public static readonly string constring = "server=.;database=DB;uid=sa;pwd=123456";
        public static DataTable GetDataTable(string Sql)
        {
            SqlConnection connection = new SqlConnection(constring);
            SqlDataAdapter adapter = new SqlDataAdapter(Sql, connection);
            DataTable dataTable = new DataTable();
            connection.Open();
            adapter.Fill(dataTable);
            return dataTable;
        }
        public static int aud(string sql)
        {
            SqlConnection connection = new SqlConnection(constring);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            return command.ExecuteNonQuery();
        }
    }
}
