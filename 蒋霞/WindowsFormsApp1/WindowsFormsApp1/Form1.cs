﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string Sql = "SELECT * FROM Users";
            var dt = Dbsql.GetDataTable(Sql);
            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string Sql;
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                Sql = "SELECT * FROM Users";

            }
            else
            {
                Sql = $"SELECT * FROM Users WHERE UserName LIKE '%{textBox1.Text}%'";
            }
            var dt = Dbsql.GetDataTable(Sql);
            dataGridView1.DataSource = dt;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var Id = (int)dataGridView1.SelectedRows[0].Cells["UserId"].Value;
                var sql = string.Format("delete from Users where UserId={0}", Id);
                var res = Dbsql.aud(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");
                    var stusql = "select * from Users";
                    var dt = Dbsql.GetDataTable(stusql);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
            }

            else
            {
                MessageBox.Show("当前未选择要删除的数据", "提示");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select *from Users";
                var dt = Dbsql.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int UserId = (int)dataGridView1.SelectedRows[0].Cells["UserId"].Value;
            string UserName = (string)dataGridView1.SelectedRows[0].Cells["UserName"].Value;
            string UserAge = dataGridView1.SelectedRows[0].Cells["UserAge"].Value.ToString();

            Form2 form2 = new Form2(UserId, UserName, UserAge);

            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select *from Users";
                var dt = Dbsql.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
        }
    }
}

