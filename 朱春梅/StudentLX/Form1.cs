﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentLX
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = selectName.Text;
            var sql = string.Format("select * from Students where Name  like  '%{0}%'",name);
            var dt = Dbhelp.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql = "select * from Students";
            var dt = Dbhelp.GetDataTable(sql);
            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dataGridView1.ReadOnly = true;

            dataGridView1.AllowUserToAddRows = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            AddForm addForm = new AddForm();
            addForm.ShowDialog();
            this.Visible = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count>0)
            {
                var r = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
                var d = MessageBox.Show("确定删除？","提示",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
                if (d.Equals(DialogResult.Yes))
                {
                    var sql = string.Format("delete from Students where Id={0}",r);
                    var dt = Dbhelp.Addupdate(sql);
                    dataGridView1.DataSource = dt;
                    MessageBox.Show("删除成功");
                }

            }
            else
            {
                MessageBox.Show("请选择一条数据删除");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["Name"].Value;
            var age = (string)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = (string)dataGridView1.SelectedRows[0].Cells["Score"].Value;
            UpdateForm updateForm = new UpdateForm(id, name, age, score);
            updateForm.Show();
        }
    }
}
