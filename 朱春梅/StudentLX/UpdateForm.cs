﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentLX
{
    public partial class UpdateForm : Form
    {
        public UpdateForm()
        {
            InitializeComponent();
        }

        private int id;
        //private string name;
        //private string age;
        //private string score;

        public UpdateForm(int id, string name, string age, string score)
        {
            InitializeComponent();
            this.id = id;
            upName.Text = name;
            upAge.Text = age;
            upScore.Text = score;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var sql = string.Format("update Students set Name='{0}',Age='{1}',Score='{2}' where Id={3}", upName.Text, upAge.Text, upScore.Text, this.id);
            Dbhelp.Addupdate(sql);
            this.Close();
            MessageBox.Show("更新成功");
        }
    }
}
