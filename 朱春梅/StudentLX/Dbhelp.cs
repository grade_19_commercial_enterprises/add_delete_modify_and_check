﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentLX
{
    class Dbhelp
    {
        private static string conString = "server=.;database=Schools;uid=sa;pwd=123456;";
        public static DataTable GetDataTable(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            connection.Open();
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        internal static int Addupdate(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();

            return command.ExecuteNonQuery();
        }
    }
}
