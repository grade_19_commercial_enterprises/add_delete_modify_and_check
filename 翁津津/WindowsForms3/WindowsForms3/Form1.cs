﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var a = "SELECT *FROM userInfo";
            var dt = DBHelper.GetDataTable(a);
            dataGridView1.DataSource = dt;
            //限制dataGridView一些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }
        //查询
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("SELECT *FROM userInfo WHERE username LIKE '%{0}%'",name);
            var dt = DBHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        //添加
        private void button3_Click(object sender, EventArgs e)
        {
            EditForm edit = new EditForm();
            var res=edit.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var a = "SELECT *FROM userInfo";
                var dt = DBHelper.GetDataTable(a);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO");
            }
        }
        //更新
        private void button2_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["userid"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["username"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["userage"].Value;
            var pwd = (string)dataGridView1.SelectedRows[0].Cells["userpwd"].Value;
            EditForm edit = new EditForm(id,name,age,pwd);
            var res=edit.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var a = "SELECT *FROM userInfo";
                var dt = DBHelper.GetDataTable(a);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO");
            }
        }
        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["userid"].Value;
                var sql = string.Format("DELETE FROM userInfo WHERE userid={0}", id);
                var res = DBHelper.AddUpdate(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功！！！", "提示");
                    var a = "SELECT *FROM userInfo";
                    var dt = DBHelper.GetDataTable(a);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败！！！！", "提示");
                }
            }
            else
            {
                MessageBox.Show("当前未选择需要删除的数据","提示");
            }
            //var row = dataGridView1.SelectedRows[0];
            //var cell = row.Cells[0];
        }
    }
}
