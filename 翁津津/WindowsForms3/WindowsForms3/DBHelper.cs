﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsForms3
{
    public class DBHelper
    {
        //连接字符串
       private static string conString = "server=.;database=Test;uid=sa;pwd=123456";

        public static DataTable GetDataTable(string sql)
        {
            //string sqlString = "SELECT *FROM userInfo";
            //声明连接对象
            SqlConnection connection = new SqlConnection(conString);

            SqlDataAdapter sqlData = new SqlDataAdapter(sql,connection);
            //打开连接
            connection.Open();

            DataTable dt = new DataTable();

            sqlData.Fill(dt);

            return dt;
        }
        public static int AddUpdate(string sql)
        {
            //声明连接对象
            SqlConnection connection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql,connection);
            //打开连接
            connection.Open();
            return command.ExecuteNonQuery();
        }
    }
}
