﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms3
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }

        private int userid;
        //private string username;
        //private int userage;
        //private int userpwd;
        public EditForm(int id, string name, int age, string pwd)
        {
            InitializeComponent();

            this.userid = id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = pwd.ToString();
        }
        //取消
        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
        //确定
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox3.Text;
            var pwd = textBox2.Text;
            //更新
            if (this.userid > 0)
            {
                var sql = string.Format("UPDATE userInfo SET username='{0}',userage={2} ,userpwd='{1}'WHERE userid={3}", name, age, pwd, this.userid);
                DBHelper.AddUpdate(sql);
                MessageBox.Show("更新成功！！！", "提示");
            }
            //添加
            else
            {
                var sql = string.Format("INSERT INTO userInfo (username,userage,userpwd) VALUES('{0}',{2},'{1}')",name,age,pwd);
                DBHelper.AddUpdate(sql);
                MessageBox.Show("添加成功！！！","提示");
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
            
        }
    }
}
