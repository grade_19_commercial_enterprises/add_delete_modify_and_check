﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = Class1.up();
            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form4 form4 = new Form4();
            form4.ShowDialog();
            dataGridView1.DataSource = Class1.up();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text))
            {
                dataGridView1.DataSource = Class1.up();
            }
            else
            {
                string sql =$"select * from Userinfo where StuName like '%{textBox1.Text}%'";
                dataGridView1.DataSource = Class1.Query(sql);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count>0)
            {
                var row = dataGridView1.SelectedRows[0].Cells[0].Value;
                string sql = $"delete from Userinfo where StuID='{row}'";
                Class1.Query(sql);
                dataGridView1.DataSource= Class1.up();
            }
            else
            { 
                MessageBox.Show("请选择要删除的数据","提示");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                string a = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                string b = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
                string c = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
                int i = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                Form5 form5 = new Form5();
                form5.hq(a,b,c,i);
                form5.ShowDialog();
                dataGridView1.DataSource = Class1.up();

            }
            else
            {
                MessageBox.Show("请选择要修改的数据", "提示");
            }


        }
    }
}
