﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }
        public int id;
        public void hq(string a,string b,string c,int i)
        {
            textBox1.Text = a;
            textBox2.Text = b;
            textBox3.Text = c;
            id = i;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(textBox2.Text) || string.IsNullOrEmpty(textBox3.Text))
            {
                MessageBox.Show("姓名、性别、地址不能为空！", "提示");
            }
            else
            {
                Class1.upds(textBox1.Text, textBox2.Text, textBox3.Text,id);
                MessageBox.Show("修改成功！", "提示");
                this.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
