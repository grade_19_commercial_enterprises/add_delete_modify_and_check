﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> User = new Dictionary<string, string>();
        private void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Focus();//让光标定位到text1上
            User.Add("zl", "132456");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text=="")
            {
                MessageBox.Show("用户名不能为空！","提示");
                textBox1.Focus();
            }
            else if(textBox2.Text=="")
            {
                MessageBox.Show("密码不能为空！","提示");
                textBox2.Focus();
            }
            else
            {
                if (User.ContainsKey(textBox1.Text) && User[textBox1.Text].Equals(textBox2.Text))
                {
                    MessageBox.Show("登录成功！","登录");
                    this.Visible = false;
                    Form3 form3 = new Form3();
                    form3.ShowDialog();
                    this.Visible = true;


                }
                else
                {
                    MessageBox.Show("账号或密码不正确！","提示");
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox1.Focus();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Form2 form2 = new Form2();
            form2.ShowDialog();
            this.Visible = true;

        }
    }
}
