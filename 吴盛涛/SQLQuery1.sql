create database Student

use Student
go

create table stuinfo
(
	id int primary key identity(1,1) not null,
	stuname nchar(10) not null,
	stuage int not null,
	stusco int not null
)

select * from stuinfo

insert into stuinfo values('张三','19','60')
insert into stuinfo values('张麻花','20','80')
insert into stuinfo values('李如花','22','90')
insert into stuinfo values('林翠花','26','70')
insert into stuinfo values('李三','19','60')
insert into stuinfo values('张麻','20','80')
insert into stuinfo values('李如','22','90')
insert into stuinfo values('林花','26','70')

select * from stuinfo where stuname like '%花%'