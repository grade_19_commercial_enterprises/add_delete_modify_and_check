﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace winform
{
    public class DbHelper
    {
        private static readonly string  conString = "server=.;database=Student;uid=sa;pwd=0520wst..";
        public static DataTable GetDataTable(string sql)
        {
            //链接数据库
            

            SqlConnection connection = new SqlConnection(conString);

            SqlDataAdapter adapter = new SqlDataAdapter(sql,connection);

            connection.Open();

            DataTable dt = new DataTable();

            adapter.Fill(dt);

            return dt;

        }

        public static int AddOrUpdate(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);

            SqlCommand command = new SqlCommand(sql, connection);

            connection.Open();

            return command.ExecuteNonQuery();
        }

    }
}
