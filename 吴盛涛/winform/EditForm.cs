﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
        }

        private readonly int id;

        public EditForm(int Id, string Name, int Age, int Score)
        {
            InitializeComponent();

            this.id = Id;
            textBox1.Text = Name;
            textBox2.Text = Age.ToString();
            textBox3.Text = Score.ToString();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var Name = textBox1.Text;
            var Age = textBox2.Text;
            var Score = textBox3.Text;

            //更新
            if (this.id > 0)
            {
                var sql = string.Format("update stuinfo set stuname='{0}',stuage={1},stusco={2}where this.id={3}", Name, Age, Score, this.id);

                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("更新成功！", "提示！");

            }
            //添加
            else 
            {
                var sql = string.Format("insert stuinfo (stuname,stuage,stusco) values('{0}',{1},{2})", Name, Age, Score);
                
                DbHelper.AddOrUpdate(sql);

                MessageBox.Show("添加成功", "提示");
            }

            this.DialogResult = DialogResult.Yes;

            this.Close();
            

        }
    }
}
