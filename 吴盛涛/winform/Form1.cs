﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“studentDataSet.stuinfo”中。您可以根据需要移动或删除它。
            
            var abc = "select * from stuinfo";

            var dt = DbHelper.GetDataTable(abc);

            dataGridView1.DataSource = dt;

            //限制dataGridView1的某些行为
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            var sql = string.Format("select * from stuinfo where stuname like '%{0}%'",name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource=dt;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res=form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var abc = "select * from stuinfo";

                var dt = DbHelper.GetDataTable(abc);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No!");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            var Id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var Name = (string)dataGridView1.SelectedRows[0].Cells["stuname"].Value;
            var Age = (int)dataGridView1.SelectedRows[0].Cells["stuage"].Value;
            var Score = (int)dataGridView1.SelectedRows[0].Cells["stusco"].Value;

            EditForm form = new EditForm(Id,Name,Age,Score);

            var res = form.ShowDialog();

            if (res == DialogResult.Yes)
            {
                var abc = "select * from stuinfo";

                var dt = DbHelper.GetDataTable(abc);

                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No!");
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0) 
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
                var sql = string.Format("delete from stuinfo where id={0}", id);

                var res = DbHelper.AddOrUpdate(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功！", "提示！");
                    var abc = "select * from stuinfo";

                    var dt = DbHelper.GetDataTable(abc);

                    dataGridView1.DataSource = dt;
                }
                else 
                {
                    MessageBox.Show("删除失败！","提示！");
                }

            }

        }
    }
}
