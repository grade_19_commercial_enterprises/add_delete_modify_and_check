﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsTest
{
    public partial class EditForm : Form
    {
        public EditForm()
        {
            InitializeComponent();
         }
        public EditForm(int id, string name, int age, int score)
        {
          InitializeComponent();
            this.id= id;
            textBox1.Text = name;
            textBox2.Text = age.ToString();
            textBox3.Text = score.ToString();
       }
        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No; 
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;

            if (this.id > 0)
            {
                var update = string.Format("update Studentinfo set name ='{0},age='1'");
                DbHelper.UpdataorSelect(update);
                MessageBox.Show("恭喜您，更新成功", "消息框");
            }
         this.DialogResult = DialogResult.Yes; 
            this.Close();
       }
    }
}
