﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“yUANDataSet.stu1”中。您可以根据需要移动或删除它。

            var TF = "select *from stu1";
            var dt = DbHelper.GetDataTable(TF);
            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from stu1 where SName like '%{0}%'",name);
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form = new Form2();
            var res =form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var TF = "select *from stu1";
                var dt = DbHelper.GetDataTable(TF);
                dataGridView1.DataSource = dt;
            }
            else 
            {
                MessageBox.Show("NO");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var SName= (String)dataGridView1.SelectedRows[0].Cells["SName"].Value;
            var iD = (int)dataGridView1.SelectedRows[0].Cells["iDDataGridViewTextBoxColumn"].Value;
            var Age = (int)dataGridView1.SelectedRows[0].Cells["ageDataGridViewTextBoxColumn"].Value;
            var Grad = (int)dataGridView1.SelectedRows[0].Cells["gradDataGridViewTextBoxColumn"].Value;
            Form2 form2 = new Form2(SName,iD,Age,Grad);
            var res= form2.ShowDialog();
            if (res == DialogResult.Yes)
            {

                var TF = "select *from stu1";
                var dt = DbHelper.GetDataTable(TF);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {

            }
            else
            {
                MessageBox.Show("当前未选择删除的数据", "提示");
            }
            var row = dataGridView1.SelectedRows.Count;
            if (row > 0)
            {
                var iD = (int)dataGridView1.SelectedRows[0].Cells["iDDataGridViewTextBoxColumn"].Value;
                var sql = string.Format("delete from Stu1 where iD={0}", iD);
                var res = DbHelper.AddOrUpdateOrDelete(sql);


                if (res == 1)
                {
                    MessageBox.Show("删除成功", "提示");
                    var TF = "select *from stu1";
                    var dt = DbHelper.GetDataTable(TF);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败", "提示");
                }
             
               
            }
            }
        }
    }

