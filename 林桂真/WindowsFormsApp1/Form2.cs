﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        private int iD;
        private int age;
        private int grad;

        public Form2()
        {
            InitializeComponent();
        }
        private int id;

        public Form2(string sName, int iD, int age, int grad)
        {
            InitializeComponent();
            this.iD = iD;
            textBox1.Text = sName;
            textBox2.Text = age.ToString();
            textBox3.Text = grad.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var SName = textBox1.Text;
            var Age = textBox2.Text;
            var Grad = textBox3.Text;
            if (this.iD > 0)
            {
                var sql = string.Format("Update Stu1 set SName='{0}',Age={1} ,Grad={2} where iD=3 ", SName, Age, Grad, this.iD);
                DbHelper.AddOrUpdateOrDelete(sql);
                MessageBox.Show("更新成功", "提示");
            }
            else
            {
                var sql = string.Format("insert into Stu1 (SName,Age,Grad) values('{0}',{1},{2})", SName, Age, Grad);
                DbHelper.AddOrUpdateOrDelete(sql);
                MessageBox.Show("添加成功", "提示");
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();

        } 
    }
}
