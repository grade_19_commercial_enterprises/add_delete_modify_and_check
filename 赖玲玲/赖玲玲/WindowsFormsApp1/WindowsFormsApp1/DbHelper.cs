﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class DbHelper
    {
        private static string conString = "sever=.;database=school;uid=sa;pwd=123456";
      

        public static DataTable GetDataTable(string sql)
        {

            SqlConnection connection = new SqlConnection(conString);


            SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
            connection.Open();


            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }
    
        public static int Addupdate(string sql)
        {
            SqlConnection Connection = new SqlConnection(conString);
            SqlCommand Command = new SqlCommand(sql, Connection);

            Connection.Open();
            return Command.ExecuteNonQuery();
        }

    }
}
