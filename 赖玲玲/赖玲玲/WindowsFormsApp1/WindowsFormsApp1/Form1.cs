﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _ = textBox1.Text;
            var sql = "select*from student";

            DataTable dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false; //限制DataGridView的某些行为


        }
        //查找
        private void button1_Click(object sender, EventArgs e)
        {

            var sql = string.Format("select*from student where studentname like'%{0}'", textBox1.Text);
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select*from student";
                var dt = DbHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;

            }
            else
            {
                MessageBox.Show("no");
            }
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = dataGridView1.SelectedRows[0].Cells["Studentname"].Value;
            var age = dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = dataGridView1.SelectedRows[0].Cells["Score"].Value;
            Form2 form2 = new Form2(id, name, age, score);
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select*from student";
                var dt = DbHelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;

            }
            else
            {
                MessageBox.Show("no");

            }


        }


        private void button4_Click(object sender, EventArgs e)
        {
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
                var sql = string.Format("delete from student where id={0}", id);


            }
        }
    }
}

