﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winform_test_001
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“sqlDataSet.demo”中。您可以根据需要移动或删除它。
            // this.demoTableAdapter1.Fill(this.sqlDataSet.demo);

            var bcd = "select * from demo";

            var preSql = "select * from Person2";

            var dt = DbHelper.GetDataTable(preSql);

         
                

            dataGridView1.DataSource = dt;

            //限制DataGridView的某些行为

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;

            var sql = string.Format("select *from Person2 where PersonName like'%{0}'", name);

            var dt = DbHelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;

        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {

            Form2 form2 = new Form2();
            var res = form2.ShowDialog();
            if(res ==DialogResult.Yes )
                {
                var preSql = "select * from Person2";

                var dt = DbHelper.GetDataTable(preSql);




                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("NO");
            }
        }
        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["PersonName"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score= (int)dataGridView1.SelectedRows[0].Cells["Score"].Value;
            Form2 form2 = new Form2(id ,name ,age, score);
            
            var res = form2.ShowDialog ();
            if (res == DialogResult.Yes)
            {
                var preSql = "select * from Person2";

                var dt = DbHelper.GetDataTable(preSql);




                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("No");
            }
            
            {

            }
        }
        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            var row = dataGridView1.SelectedRows[0];

            var cell = row.Cells[0];

            var id = (int)dataGridView1.SelectedRows[0].Cells["Id"].Value;

            var sql = string.Format("delete from Person2 where Id={0}", id );

            var res = DbHelper.AddOrUpdate(sql);

            if (res == 1)
            {
                MessageBox.Show("删除成功");

                var preSql = "select * from Person2";

                var dt = DbHelper.GetDataTable(preSql);




                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("删除失败");
            }

        }
    }
}
