﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Winform_test_001
{
    public partial class Form2 : Form
    {
        public  int id;
     

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(int id, string name, int age, int score)
        {

            InitializeComponent();
            this.id = id;
           textBox1 .Text  = name;
            textBox2 .Text  = age.ToString ();
            textBox3.Text   = score.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            var name = textBox1.Text;
            var age = textBox2.Text;
            var score = textBox3.Text;
            if (this .id > 0)
            {
                var sql =
                   string.Format("update Person2 set PersonName='{0}',Age={1},Score={2} where Id={3}", name, age, score, this.id);

                DbHelper.AddOrUpdate(sql);
                MessageBox.Show("更新成功");
            }
            else
            {
                var sql = 
                    string.Format("insert into  Person2 (PersonName,Age,Score) values ('{0}',{1},{2})",name ,age ,score );

                DbHelper.AddOrUpdate(sql);
                MessageBox.Show("添加成功");
                
            }
            this.DialogResult = DialogResult.Yes ;
            this.Close();
            
        }
    }
}
