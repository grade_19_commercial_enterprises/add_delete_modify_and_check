﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“dataSet1.student”中。您可以根据需要移动或删除它。
            this.studentTableAdapter.Fill(this.dataSet1.student);
          

            var stusql = "select * from teacher ";
            var dt = DBhelper.GetDataTable(stusql);

            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
        }
//更新
        private void button3_Click(object sender, EventArgs e)
        {
            var id = dataGridView1.SelectedRows[0].Cells["id"].Value;
            var name = dataGridView1.SelectedRows[0].Cells["studentname"].Value;
            var age= dataGridView1.SelectedRows[0].Cells["Age"].Value;
            var score = dataGridView1.SelectedRows[0].Cells["Score"].Value;
            editform form = new editform(id,name,age,score);  

            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name=textBox1.Text;
            var sql= string.Format("select *from teacher  where studentname like '%{0}%'",name);
            var dt = DBhelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            editform form = new editform();
            form.Show();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
