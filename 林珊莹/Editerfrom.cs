﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace praticeWinforms
{
    public partial class Editerfrom : Form
    {
        public Editerfrom()
        {
            InitializeComponent();
        }

        private int studentID ;

        public Editerfrom(int studentID,string studentname,string studentsex,int studentage,string studentscore)
        {
            InitializeComponent();
            this.studentID = studentID;
            textBox1.Text = studentname;
            textBox2.Text = studentage.ToString();
            textBox3.Text = studentscore;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var studentname = textBox1.Text;
            var studentsex = textBox2.Text;
            var studentage = textBox3.Text;
            var studentscore = textBox4.Text;
            if (this.studentID > 0)
            {
                var sql = string.Format("update StudentInfo set StudentName='{0}',StudentSex='{1}',StudentAge={2} ,StudentScore='{3}' where StudentID={4} ", studentname, studentsex, studentage, studentscore, this.studentID);
                DBHelper.AddUpdateordelete(sql);
                MessageBox.Show("恭喜您，更新成功", "信息提示框");
            }
            else 
            {
                var sql = string.Format("insert into StudentInfo (StudentName,StudentSex,StudentAge,StudentScore) values ('{0}','{1}',{2},'{3}')", studentname, studentsex, studentage, studentscore);
                DBHelper.AddUpdateordelete(sql);
                MessageBox.Show("恭喜您，添加成功", "信息提示框");
            }
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
    }
}
