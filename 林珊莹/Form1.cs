﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace praticeWinforms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var abc = "select * from Grade";
            var Studentsql = "select * from StudentInfo";
            var dt = DBHelper.GetdataTable(Studentsql);
            dataGridView1.DataSource = dt;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;//限制封装
            dataGridView1.ReadOnly = true;//限制编辑
            dataGridView1.AllowUserToAddRows = false;//限制下一行
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var studentname = textBox1.Text;
            var sql = string.Format("select * from StudentInfo where StudentName like '%{0}%'", studentname);
            var dt = DBHelper.GetdataTable(sql);
            dataGridView1.DataSource = dt;
        }

        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button2_Click(object sender, EventArgs e)
        {
            Editerfrom editerfrom = new Editerfrom();
            var res=editerfrom.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var Studentsql = "select * from StudentInfo";
                var dt = DBHelper.GetdataTable(Studentsql);
                dataGridView1.DataSource = dt;
            }
            else 
            {
                MessageBox.Show("No");
            }
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var studentID = (int)dataGridView1.SelectedRows[0].Cells["StudentID"].Value;
                var studentname = (string)dataGridView1.SelectedRows[0].Cells["StudentName"].Value;
                var studentsex = (string)dataGridView1.SelectedRows[0].Cells["StudentSex"].Value;
                var studentage = (int)dataGridView1.SelectedRows[0].Cells["StudentAge"].Value;
                var studentscore = (string)dataGridView1.SelectedRows[0].Cells["StudentScore"].Value;
                Editerfrom editerform = new Editerfrom(studentID, studentname, studentsex, studentage, studentscore);
                var res = editerform.ShowDialog();
                if (res == DialogResult.Yes)
                {
                    var Studentsql = "select * from StudentInfo";
                    var dt = DBHelper.GetdataTable(Studentsql);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("No");
                }
            }
            else
            {
                MessageBox.Show("对不起，您为选中要更新的对象", "信息提示框");
            }
            
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var studentID = (int)dataGridView1.SelectedRows[0].Cells["StudentID"].Value;
                var sql = string.Format("delete from StudentInfo where StudentID={0}", studentID);
                var res = DBHelper.AddUpdateordelete(sql);
                if (res == 1)
                {
                    MessageBox.Show("删除成功", "信息提示框");
                    var Studentsql = "select * from StudentInfo";
                    var dt = DBHelper.GetdataTable(Studentsql);
                    dataGridView1.DataSource = dt;
                }
                else
                {
                    MessageBox.Show("删除失败", "信息提示框");
                }
            }
            else 
            {
                MessageBox.Show("对不起，您没有选择当前要选择的项","信息提示框");
            }
            
         }


    }
}